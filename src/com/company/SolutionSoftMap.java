package com.company;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SolutionSoftMap<K, V> extends AbstractMap<K, V> {
    private Map<K, SoftValue<V>> solutions;
    private ReferenceQueue<V> queue = new ReferenceQueue<V>();

    /** Liczba odwołań */
    public Integer appeals = 0;

    /** Liczba odwołań nieudanych (klucz nieistnieje)*/
    public Integer failedAppeals = 0;

    public SolutionSoftMap() {
        solutions = new HashMap<K, SoftValue<V>>();
    }

    public synchronized V get(Object key) {
        processQueue();
        SoftReference<V> o = solutions.get(key);
        if (o == null) {
            return null;
        }
        return o.get();
    }

    public synchronized V put(K key, V value) {
        processQueue();
        SoftValue<V> old = solutions.put(key, new SoftValue<V>(value, queue, key));
        return old == null ? null : old.get();
    }

    public synchronized V remove(Object key) {
        processQueue();
        SoftReference<V> ref = solutions.remove(key);
        return ref == null ? null : ref.get();
    }

    public synchronized void clear() {
        processQueue();
        solutions.clear();
    }

    @Override
    public synchronized boolean containsKey(Object key) {
        appeals++;

        if (!solutions.containsKey(key))
        {
            failedAppeals++;
            return false;
        }

        if(solutions.get(key) == null)
        {
            return false;
        }

        return true;
    }

    public Float ratio() {
        return (float) failedAppeals / (float) appeals * 100;
    }

    public int size() {
        return solutions.size();
    }

    public Set<Entry<K, V>> entrySet() {
        throw new UnsupportedOperationException();
    }

    @SuppressWarnings("unchecked")
    private void processQueue() {
        while (true) {
            Reference<? extends V> o = queue.poll();
            if (o == null) {
                return;
            }
            SoftValue<V> k = (SoftValue<V>) o;
            Object key = k.key;
            solutions.remove(key);
        }
    }

    private static class SoftValue<T> extends SoftReference<T> {
        final Object key;

        public SoftValue(T ref, ReferenceQueue<T> q, Object key) {
            super(ref, q);
            this.key = key;
        }
    }
}
