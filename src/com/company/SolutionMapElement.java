package com.company;
import App.Solution;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;

public class SolutionMapElement {
    public Solution solution;
    public String string;

    public SolutionMapElement(Solution solution) {
        String string = new String().format("%50000s", "").replace(' ', 't');

        this.string = string;
        this.solution = solution;
    }
}
