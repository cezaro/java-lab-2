package com.company;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, MalformedURLException {
        ArrayList<Class> algorithms = new ArrayList<Class>();

        final int threadsCount = 10;

        // loading algorithms
        final URL algorithmsFolder = new File("cw1/").toURI().toURL();
        final File folder = new File("cw1/Algorithms/");

        for (final File fileEntry : folder.listFiles()) {
            String className = fileEntry.getName().replace(".class", "");
            Class algorithm = new URLClassLoader(new URL[]{algorithmsFolder}).loadClass("Algorithms." + className);

            for(Method method : algorithm.getMethods())
            {
                if(method.getName().equals("findSolution"))
                {
                    algorithms.add(algorithm);
                }
            }
        }

//        SolutionMap<Long, SolutionMapElement> map = new SolutionMap<>();
        SolutionSoftMap<Long, SolutionMapElement> map = new SolutionSoftMap<>();
        Thread [] threads = new Thread[threadsCount];

        for(int i = 0; i < threadsCount; i++)
        {
            threads[i] = new SolutionThread(i, map, algorithms);
            threads[i].start();
        }

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Rozmiar mapy: " + map.size() + " " + map.ratio());
            }
        }, 0, 1000);
    }
}
