package com.company;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SolutionMap<Long, SolutionMapElement> extends AbstractMap<Long, SolutionMapElement> {
    private Map<Long, SolutionMapElement> solutions;

    /** Liczba odwołań */
    public Integer appeals = 0;

    /** Liczba odwołań nieudanych (klucz nieistnieje)*/
    public Integer failedAppeals = 0;

    public SolutionMap() {
        solutions = new HashMap<Long, SolutionMapElement>();
    }

    @Override
    public Set<Entry<Long, SolutionMapElement>> entrySet() {
        throw new UnsupportedOperationException();
    }

    public synchronized SolutionMapElement get(Object key) {
        SolutionMapElement o = solutions.get(key);
        if (o == null) {
            return null;
        }
        return o;
    }

    public synchronized SolutionMapElement put(Long key, SolutionMapElement value) {
        SolutionMapElement old = solutions.put(key, value);
        return old == null ? null : old;
    }

    public synchronized SolutionMapElement remove(Object key) {
        SolutionMapElement ref = solutions.remove(key);
        return ref == null ? null : ref;
    }

    public synchronized void clear() {
        solutions.clear();
    }

    @Override
    public synchronized boolean containsKey(Object key) {
        appeals++;

        if (!solutions.containsKey(key))
        {
            failedAppeals++;
            return false;
        }

        if(solutions.get(key) == null)
        {
            return false;
        }

        return true;
    }

    public Float ratio() {
        return (float) failedAppeals / (float) appeals * 100;
    }

    public int size() {
        return solutions.size();
    }
}
