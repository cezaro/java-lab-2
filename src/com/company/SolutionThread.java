package com.company;

import App.Instance;
import App.Solution;

import java.lang.ref.SoftReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Random;

public class SolutionThread extends Thread {
    private Integer id;
    private AbstractMap map;
    private ArrayList<Class> algorithms;

    public SolutionThread(Integer id, AbstractMap map, ArrayList algorithms)
    {
        this.id = id;
        this.map = map;
        this.algorithms = algorithms;
    }

    public void run() {
        while(true)
        {
            Long seed = Generator.generateSeed();
            Integer rnd = new Random().nextInt(algorithms.size());

            if (map.containsKey(seed)) {
                continue;
            }

            Class alg = algorithms.get(rnd);

            SoftReference<SolutionMapElement> ref = null;
            Method findSolution = null;
            Solution sol = null;
            try {
                findSolution = alg.getMethod("findSolution");
                Constructor constructor = alg.getDeclaredConstructor(Instance.class);

                Object o = constructor.newInstance(Generator.getInstance(seed));

                sol = (Solution) findSolution.invoke(o);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }

            map.put(seed, new SolutionMapElement((Solution) sol));

//            System.out.println("Wątek " + id + " " + alg.getName());
            /*try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
        }
    }
}
