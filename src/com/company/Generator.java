package com.company;

import App.Instance;
import App.Item;

import java.util.ArrayList;
import java.util.Random;

public class Generator {
    public Long seed;
    public Integer knapsackSize;
    public Integer itemsCount;

    public Generator() {
        seed = generateSeed();
        knapsackSize = getKnapsackSize(this.seed);
        itemsCount = getItemsCount(this.seed);
    }

    public static Long generateSeed()
    {
        Random generator = new Random();

        String rnd = "";
        for(int i = 0; i < 18; i++)
        {
            int digit = generator.nextInt(10);

            if(i < 14)
                digit = 1;

            rnd = rnd + digit;
        }

        return Math.abs(generator.nextLong() % 1000);
    }

    public static Integer getKnapsackSize(Long seed) {
        return Integer.parseInt((seed % 57) + 10 + "");
    }

    public static int getItemsCount(Long seed) {
        Random generator = new Random(seed);

        return generator.nextInt(20) + 1;
    }

    public static ArrayList<Item> generateItems(Long seed) {
        ArrayList<Item> items = new ArrayList<Item>();

        Random generator = new Random(seed);

        for(int i = 1; i <= getItemsCount(seed); i++)
        {
           items.add(new Item(generator.nextInt(getKnapsackSize(seed)) + 1, generator.nextFloat() * 100));
        }

        return items;
    }

    public static Instance getInstance(Long seed) {
        return new Instance(generateItems(seed), getKnapsackSize(seed));
    }
}
